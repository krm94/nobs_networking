#ifndef NOBS_NETWORKING_H
#define NOBS_NETWORKING_H

#define NOBS_BUFFER_SIZE 1024
#define NOBS_SYNC_1 0xAA
#define NOBS_SYNC_2 0xFE
#define NOBS_SYNC_3 0x83
#define NOBS_SYNC_4 0x02

#include <stdlib.h>

static const char nobs_sync_seq[4] = {0xAA, 0xFE, 0x83, 0x02};

struct nobs_config_t {
  int server_fd;
  int socket;
  struct sockaddr_in* address;
  char* buffer;
  int buffer_size;
};

struct nobs_frame_t {
  char* data;
  int size;
};

struct nobs_config_t* nobs_networking_server_init(int port);
int nobs_networking_server_listen(struct nobs_config_t* config);
struct nobs_config_t* nobs_networking_client_init(char* addr, int port);
int nobs_networking_client_connect(struct nobs_config_t* config);
struct nobs_frame_t* nobs_networking_rx_frame(struct nobs_config_t* config);
int nobs_networking_tx_frame(struct nobs_config_t* config, struct nobs_frame_t* frame);
struct nobs_frame_t* nobs_networking_frame_init(int size);
void nobs_networking_free_frame(struct nobs_frame_t* frame);
void nobs_networking_close(struct nobs_config_t* config);


#endif
