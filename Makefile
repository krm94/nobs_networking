cppsrc = nobs_test.cpp
cppobj = $(cppsrc:.cpp=.o)

LDFLAGS =
CXXFLAGS =

nobs_test: $(cppobj)
	$(CXX) -o $@ $^ $(LDFLAGS) $(CXXFLAGS)

.PHONY: clean
clean:
	rm -f $(obj) gs
