#include <iostream>

#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

class NoBS_Server {
public:
  NoBS_Server(int port) : m_error(false), m_port(port),
    m_server_started(false) {}
  ~NoBS_Server();
  bool start()
  {
    int addrlen = sizeof(m_address);

    if ((m_server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
      m_error = true;
      return false;
    }

    int opt = 1;
    if (setsockopt(m_server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
      &opt, sizeof(opt)))
    {
      m_error = true;
      return false;
    }

    m_address.sin_family = AF_INET;
    m_address.sin_addr.s_addr = INADDR_ANY;
    m_address.sin_port = htons(m_port);

    if (bind(m_server_fd, (struct sockaddr*)&m_address, sizeof(m_address)))
    {
      m_error = true;
      return false;
    }

    if (listen(m_server_fd, 3) < 0)
    {
      m_error = true;
      return false;
    }

    return true;
  }
  void listen()
  {

  }
protected:
  int m_port;
  int m_server_fd;
  int m_valread;
  std::list<int> m_sockets;
  struct sockaddr_in m_address;
  char m_buffer[1024];
  std::atomic<bool> m_error;
  std::atomic<bool> m_server_started;
};

class NoBS_Client {
public:
  NoBS_Client();
  ~NoBS_Client();
protected:
};

class NoBS_Packet {
public:
  NoBS_Packet();
  ~NoBS_Packet();
protected:
}
