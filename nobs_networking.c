#include "nobs_networking.h"

#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <errno.h>

struct nobs_config_t* nobs_networking_server_init(int port)
{
  struct nobs_config_t* config =
    (struct nobs_config_t*)malloc(sizeof(struct nobs_config_t));

  config->address = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));

  config->buffer = malloc(sizeof(char)*NOBS_BUFFER_SIZE);

  if ((config->server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
  {
    printf("NOBS Error 1\n");
    return NULL;
  }

  bzero((char *) config->address, sizeof(*config->address));


  int opt = 1;
  if (setsockopt(config->server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
    &opt, sizeof(opt)))
  {
    printf("NOBS Error 2\n");
    return NULL;
  }

  config->address->sin_family = AF_INET;
  config->address->sin_addr.s_addr = INADDR_ANY;
  config->address->sin_port = htons(port);

  int r = bind(config->server_fd, (struct sockaddr*)config->address, sizeof(*config->address));
  if (r < 0)
  {
    printf("NOBS Error 3: %i, %i\n", r, errno);
    return NULL;
  }

  return config;
}

int nobs_networking_server_listen(struct nobs_config_t* config)
{
  if (listen(config->server_fd, 3) < 0)
  {
    printf("NOBS Error 4\n");
    return -1;
  }

  printf("NOBS: Listening for connections\n");

  unsigned int addrlen = sizeof(*config->address);
  if ((config->socket = accept(config->server_fd, (struct sockaddr*)config->address,
    &addrlen)) < 0)
  {
    printf("NOBS Error 5, %i\n", errno);
    return -1;
  }

  printf("NOBS: Connection Accepted!\n");

  return 0;
}

struct nobs_config_t* nobs_networking_client_init(char* addr, int port)
{
  struct nobs_config_t* config = malloc(sizeof(struct nobs_config_t));
  config->buffer = malloc(sizeof(char) * NOBS_BUFFER_SIZE);
  config->address = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));

  //bzero(&config->address, sizeof(*config->address));

  (*config->address).sin_port = htons(port);
  (*config->address).sin_family = AF_INET;

  if ( inet_aton(addr, (struct in_addr*)&config->address->sin_addr.s_addr) == 0 )
  {
    printf("Address ERROR!\n");
    return NULL;
  }

  if ( (config->socket = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
  {
    printf("Error creating socket!\n");
    return NULL;
  }

  return config;
}

int nobs_networking_client_connect(struct nobs_config_t* config)
{
  if( connect(config->socket, (struct sockaddr*)config->address, sizeof(*config->address)) != 0 )
  {
    printf("Error connecting to server!, %i\n", errno);
    return -1;
  }

  return 0;
}

struct nobs_frame_t* nobs_networking_rx_frame(struct nobs_config_t* config)
{

  int header_received = 0;
  int pos_in_seq = 0;
  while(!header_received)
  {
    size_t n = read(config->socket, config->buffer, 1);

    if (n == 1)
    {
      if (config->buffer[0] == nobs_sync_seq[pos_in_seq])
      {
        if(pos_in_seq == 3)
        {
          header_received = 1;
        }
        else
        {
          pos_in_seq++;
        }
      }
      else
      {
        pos_in_seq = 0;
      }
    }
  }

  size_t received = 0;
  while(received < 2)
  {
    size_t n = read(config->socket, (config->buffer + received), (2 - received));
    if (n <= 0)
    {
      break;
    }
    received += n;
  }

  int length = ((int)config->buffer[0] << 8) | (int)config->buffer[1];

  received = 0;
  while(received < length)
  {
    size_t n = read(config->socket, (config->buffer + received), (length - received));
    if (n <= 0)
    {
      break;
    }
    received += n;
  }

  struct nobs_frame_t* frame = malloc(sizeof(struct nobs_frame_t));
  frame->data = malloc(sizeof(char)*length);
  frame->size = length;

  memcpy(frame->data, config->buffer, length);

  return frame;
}

int nobs_networking_tx_frame(struct nobs_config_t* config, struct nobs_frame_t* frame)
{
  size_t n = write(config->socket, nobs_sync_seq, 4);
  if(n < 0)
  {
    printf("Error writing to socket!\n");
    return -1;
  }

  char length[2];
  length[0] = (frame->size >> 8);
  length[1] = frame->size;

  n = write(config->socket, length, 2);
  if(n < 0)
  {
    printf("Error writing to socket!\n");
    return -1;
  }

  n = write(config->socket, frame->data, frame->size);
  if(n < 0)
  {
    printf("Error writing to socket!\n");
    return -1;
  }

  return 0;
}

struct nobs_frame_t* nobs_networking_frame_init(int size)
{
  struct nobs_frame_t* frame = malloc(sizeof(struct nobs_frame_t));
  frame->data = malloc(size);
  frame->size = size;

  return frame;
}

void nobs_networking_free_frame(struct nobs_frame_t* frame)
{
  free(frame->data);
  free(frame);
  frame = NULL;

  return;
}

void nobs_networking_close(struct nobs_config_t* config)
{
  close(config->socket);

  free(config->buffer);
  free(config->address);

  config = NULL;

  return;
}
